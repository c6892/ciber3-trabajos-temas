%Aplicación red neuronal FF para prestamo y plazo bancario
close all
clear all
warning off

%Rango de los datos de entrada
R = [0 75; 
     0 750;
     0 205];

% %% Configuracion 1
%
% %Configuración capas y neuronas 
% S=[25 27 25 25 1];
% 
% %Red neuronal FF para plazo
% net = newff(R,S,{'tansig','hardlim','tansig','tansig','purelin'});
% %Red neuronal FF para prestamo
% net2 = newff(R,S,{'tansig','hardlim','tansig','tansig','purelin'});

%% Configuracion 2

%Configuración capas y neuronas 
S=[40 35 40 1];

%Red neuronal FF para plazo
net = newff(R,S,{'hardlim','tansig','purelin','purelin'});
%Red neuronal FF para prestamo
net2 = newff(R,S,{'hardlim','tansig','purelin','purelin'});

% %% Configuracion 3
% %Configuración capas y neuronas 
% S=[70 70 1];
% 
% %Red neuronal FF para plazo
% net = newff(R,S,{'hardlim','hardlim','purelin'});
% %Red neuronal FF para prestamo
% net2 = newff(R,S,{'hardlim','hardlim','purelin'});

% %% Configuracion 4
% %Configuración capas y neuronas 
% S=[120 1];
% 
% %Red neuronal FF para plazo
% net = newff(R,S,{'tansig','purelin'});
% %Red neuronal FF para prestamo
% net2 = newff(R,S,{'tansig','purelin'});

%% Codigo
%Datos entrada
EEdad = xlsread('datosBanco.xlsx','Analisis','A3:A53');
EIngreso = xlsread('datosBanco.xlsx','Analisis','B3:B53');
ECapPago = xlsread('datosBanco.xlsx','Analisis','C3:C53');
%Unimos cada dato con la entrada
entradas = transpose([EEdad EIngreso ECapPago])

%Datos y salida
SPlazo = transpose(xlsread('datosBanco.xlsx','Analisis','D3:D53'));
SPrestamo = transpose(xlsread('datosBanco.xlsx','Analisis','E3:E53'));

%Simulación sin entrenar
Y = sim(net,entradas)
Y2 = sim(net2,entradas)

%Entrenamiento para plazo
net.trainParam.min_grad=0;
net = train(net,entradas,SPlazo);

%entrenamiento para prestamo
net2.trainParam.min_grad=0;
net2 = train(net2,entradas,SPrestamo);

%eje x
t = [1:51]
%Grafico simulación v real (Plazo)
Y = sim(net,entradas);
display(Y)
plot(t,SPlazo)
hold on
plot(t,Y)
hold off
title('Plazo real v simulado')
legend('plazo real','plazo simulado')

%Grafico simulación v real (Prestamo)
Y2 = sim(net2,entradas);
plot(t,SPrestamo,t,Y2)
hold off
title('Prestamo real v simulado')
legend('Prestamo real','Prestamo simulado')

%Figura del error plazo
e = SPlazo-Y;
display(e)
figure
plot(e)
legend('Error Plazo')
title('Grafico de error plazo')

%Figura del error prestamo
e2=SPrestamo-Y2;
figure
plot(e2)
legend('Error Prestamo')
title('Grafico de error prestamo')

%Valor del MSE plazo
msePlazo = (1/length(e))*sum(e.^2)

%Valor del MSE prestamo
msePrestamo = (1/length(e2))*sum(e2.^2)