%Ejemplo del sistema de l�gica difusa de la propina
%Esta conf. se tiene un conjunto mas grande de consideracion
%para las personas adultas no adultos mayores
%se mantienen reglas, ingresos bajos 300 - 400, medios 400-800, altos 650-1000
%implementado en comandos

close all
clear all
warning('off')

%Sistema
a=newfis('Credito Bancario');

%Variable de entrada: Cliente
a=addvar(a,'input','Cliente',[15 74]);

%Funciones de pertenencia
a=addmf(a,'input',1,'Joven','gaussmf',[2 366 25]);
a=addmf(a,'input',1,'Adulto','gaussmf',[7 45 45]);
a=addmf(a,'input',1,'Viejo','gaussmf',[2 36 67]);
%plotmf(a,'input',1)

%Variable de entrada: Ingresos
a=addvar(a,'input','Ingresos',[200 1000]);

%Funciones de pertenencia
a=addmf(a,'input',2,'Bajos','gaussmf',[50 400]);
a=addmf(a,'input',2,'Medios','gaussmf',[50 600]);
a=addmf(a,'input',2,'Altos','gaussmf',[50 825]);
%a=addmf(a,'input',2,'Buena','trapmf',[7 8 20 20]);
%plotmf(a,'input',2)

%Variable de entrada: Capacidad de Pago
a=addvar(a,'input','Capacidad_Pago',[20 300]);

%Funciones de pertenencia
a=addmf(a,'input',3,'Minimo','gaussmf',[20 75]);
a=addmf(a,'input',3,'Promedio','gaussmf',[20 150]);
a=addmf(a,'input',3,'Maximo','gaussmf',[20 200]);
%a=addmf(a,'input',2,'Buena','trapmf',[7 8 20 20]);
%plotmf(a,'input',3)

%Variable de salida: Plazo
a=addvar(a,'output','Plazo',[0 45]);

%Funciones de pertenencia
a=addmf(a,'output',1,'Corto','trimf',[2 12 20]);
a=addmf(a,'output',1,'Prolongado','trimf',[17 30 45]);
%a=addmf(a,'output',1,'Buena','trimf',[13 15 17]);
%plotmf(a,'output',1)

%Variable de salida: Prestamo
a=addvar(a,'output','Prestamo',[0 25]);

%Funciones de pertenencia
a=addmf(a,'output',2,'Bajo','trimf',[0 3 6]);
a=addmf(a,'output',2,'Promedio','trimf',[5 11 18]);
a=addmf(a,'output',2,'Alto','trimf',[16 19 22]);
%Los valores estan dados en numero de salarios minimos legales vigentes 

%Reglas de inferencia
ruleList=[
  	1 0 0 1 0 1 2
   	2 0 0 2 0 1 2
    3 0 0 1 0 1 2
    0 1 1 0 1 1 2
    0 2 2 0 2 1 2
    0 3 3 0 3 1 2];

a = addrule(a,ruleList);

%Sistema difuso
fuzzy(a)

%Evaluar el sistema
%Individual
%Y = evalfis([29 650 100],a)

%Para evaluar varias entradas
Datos = [37 248 50
    34 350 60
    46 248 50
    44 480 110
    30 350 50
    48 600 150
    22 248 20
    53 500 100
    21 248 50
    43 800 200
    25 700 90
    26 248 10
    68 600 120
    41 450 90
    27 248 30
    50 600 205
    24 248 50];

%Varios
Y = evalfis(Datos,a)
writematrix(Y,'TC2.xlsx','sheet','WriteMAtrix','Range','A5:B21')
